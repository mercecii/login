import React from 'react';
import { FormBlock, InputBlock, ButtonBlock } from '../../common-components';

export default function Login(props) {
    return (
        <div>
            This is Login Screen
            <FormBlock >
                <InputBlock
                    label="Email"
                    type="email"
                    placeholder="Enter your Email"
                    inputClass=""
                />
                <InputBlock
                    label="Password"
                    type="password"
                    placeholder="Enter your Password"
                    btnClass=""
                />
                <ButtonBlock
                    label="Submit"

                />
            </FormBlock>
        </div>
    );
}
